package se325.lab01.concert.client;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.LocalDateTime;
import java.util.List;

import se325.lab01.concert.common.Concert;

import org.junit.BeforeClass;
import org.junit.Test;
import se325.lab01.concert.common.ConcertService;

/**
 * JUnit test client for the RMI whiteboard application.
 */
public class Client {

    // Proxy object to represent the remote ShapeFactory service.
    private static ConcertService proxy;

    /**
     * One-time setup method to retrieve the ShapeFactory proxy from the RMI
     * Registry.
     */
    @BeforeClass
    public static void getProxy() {
        try {
            // Instantiate a proxy object for the RMI Registry, expected to be
            // running on the local machine and on a specified port.
            Registry lookupService = LocateRegistry.getRegistry("localhost", 10000);

            // Retrieve a proxy object representing the ShapeFactory.
            proxy = (ConcertService) lookupService.lookup("concertService");
        } catch (RemoteException e) {
            System.out.println("Unable to connect to the RMI Registry");
        } catch (NotBoundException e) {
            System.out.println("Unable to acquire a proxy for the Concert service");
        }
    }

    /**
     * Test that, using the ShapeFactory proxy, we can invoke methods on the
     * remote ShapeFactory to create remotely accessible Shapes. This test also
     * then invokes methods on the remote Shapes objects, via their acquired
     * proxies.
     */
    @Test
    public void testCreate() throws RemoteException {
        try {
            LocalDateTime now = LocalDateTime.now();
            // Use the ShapeFactory proxy to create a couple of remote Shape
            // instances. newShape() returns proxies for the new remote Shapes.
            Concert concert = proxy.createConcert(new Concert("Example concert", now));

            // Query the remote factory.
            List<Concert> remoteConcerts = proxy.getAllConcerts();

            assertTrue(remoteConcerts.contains(concert));
            assertEquals(1, remoteConcerts.size());

        } catch (Exception e) {
            fail();
        }
    }
}

