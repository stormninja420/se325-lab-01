package se325.lab01.concert.server;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import se325.lab01.concert.common.Concert;
import se325.lab01.concert.common.ConcertService;
/**
 * Simple Java RMI server that creates a remotely accessible ShapeFactory
 * object and registers it with RMI's Naming service.
 */
public class Server {

    public static void main(String[] args) {
        try {

            // Create the Registry on the localhost.
            Registry lookupService = LocateRegistry.createRegistry(10000);

            // Instantiate ShapeFactoryServant.
            ConcertService service = new ConcertServiceServant();

            // Advertise the ShapeFactory service using the Registry.
            lookupService.rebind("concertService", service);

            System.out.println(
                    "Server running");

        } catch (RemoteException e) {
            System.out.println("Unable to start or register proxy with the RMI Registry");
            e.printStackTrace();
        }
    }
}
